#!/usr/bin/env python3

import sys
import click
import shutil
import re


@click.command()
@click.argument('fin', metavar='input', type=click.File('r'))
@click.argument('fout', metavar='output', type=click.File('w'))
def finalizer(fin, fout):
    # read input
    tin = ''.join(fin.readlines())

    # regexp object for finding the `documentclass` header
    reg = re.compile(r'documentclass(\[([\%a-zA-Z0-9=,\ \r\n]+)\])?{([a-zA-Z0-9\-_]+)}')
    # append "final" to the document class options
    tout = reg.sub(r'documentclass[\2final,%\n]{\3}', tin)
    
    # write output
    fout.writelines(tout)


if __name__ == "__main__":
    finalizer()
